#!/bin/bash

# transform parameters file if GET
if [ "$2" = 'GET' ]
then sed -i "s/\&/\n/g" $3
fi

source "${0%/*}"/config # load configuration

if [ "$timer" = true ]
then start_time=$(($(date +%s%N)/1000000))
fi

boolean_value () {
    echo "$1 ="
    if [ "$2" = true ]
    then echo "true"
    else echo "false"
    fi
    echo "<br/>"
}

if [ "$debug" = true ]
then echo "<span style='color:red'>parameters: $1 $2 $3</span>"
fi

method=$2 # GET or POST

args=$3 # the name of a data file

#
# transformation of an alternation of html/ocam blocks into full ocaml code (html block are simply printed)
#
function prepare_code {
    # normalize <?ocaml and ?> position, alone and at the beginning of its line
    sed "s/<?ocaml/\n<?ocaml\n;;\n/g" $1.ocaml > $1.prep
    sed -i "s/?>/\n?>\n;;\n/g" $1.prep
    # a priori out of ocaml code
    in=0
    while IFS= read -r line # read line (from $1.prep file)
    do
	if [[ $line = "<?ocaml" ]]
	then in=1 # we're entering the ocaml code
	else 
	    if [[ $line = "?>" ]]
	    then in=0 # we're getting out of the ocaml code
	    else
		if [[ $in = 0 ]] # for html code, simply print
		then
		    line="$(echo $line | sed 's/\\/\\\\/g')"
		    line="$(echo $line | sed 's/\"/\\\"/g')"
		    echo "print_string(\"$line\"^\"\n\");"
		else 
		    echo $line # else, let it in ocaml
		fi
	    fi
	fi
    done < $1.prep # file to read
}

if [[ ! -f "$1" ]] 
then
    echo "file <span style='color:red'><b>$1</b> not found!</span><br/>"
else
    file=$1
#    echo "file avant $file<br/>"
#    file=$(echo "$file" | sed 's/\.ocaml(.*)$/.ocaml/')
#    echo "file après $file<br/>"
    file=${file%%.*}
    if [ "$compile" = false ];
    then
	if [ "$file.ml" -ot "$file.ocaml" ] # if prepared code older than source
	then
       	    rm $file.ml
	    prepare_code $file > $file.ml 2>&1
	    if [ $debug = true ];
	    then echo "<span style='color:red'>prepare code</span><br/>"
	    fi	    
	fi
	ocaml $file.ml
	if [ "$debug" = true ];
	then echo "<br/><span style='color:red'>[using interpreted version]</span><br/>"
	else rm $1.prep
	fi
    else
	if [ "$file" -ot "$file.ocaml" ] # if compiled code older than source
	then
	    rm $file.ml $file
	    prepare_code $file > $file.ml 2>&1
	    ocamlc $file.ml -o $file 2>&1 # compilation of file
	    if [ "$debug" = false ];
	    then rm $file.cmi $file.cmo $file.prep
	    fi
	fi
	$file $method $args 2>&1 # exécution of file
	if [ "$debug" = true ];
	then echo "<br/><span style='color:red'>[using compiled version]</span><br/>"
	fi
    fi
fi

if [ "$timer" = true ]
then
    end_time=$(($(date +%s%N)/1000000))
    time_cost=$(($end_time - $start_time))
    echo "<span style='color:green'><b>time cost = $time_cost ms</b></span><br/>"
fi

if [ "$debug" = true ];
then
    echo "<span style='color:red'"
    boolean_value timer $timer
    boolean_value debug $debug
    boolean_value compile $compile
    echo "method = $method<br/>"
    echo "passed arguments are in file <b>$args</b><br/>"
    echo "<pre>"
    cat $args   
    echo "</pre></span>"
fi
if [ "$debug" = false ]
then rm $3
fi
