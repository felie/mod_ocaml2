/* 
 *  mod_ocaml2.c 
 *  © François Elie & Marin Elie 2020
 *  see LICENSE (GPL)
 *
 *  to compile:
 *  $ apxs -c -i mod_ocaml2.c
 *
 *  Add these directives to apache2.conf:
 *     LoadModule ocaml2_module /usr/lib/apache2/modules/mod_ocaml2.so 
 *     <FilesMatch ".+\.ocaml">
 *       SetHandler ocaml2
 *     </FilesMatch>
 * 
 *  Then restart apache2 with
 *  $ sudo service apache2 restart
 * 
 *  You can request *.ocaml files and watch for the result
 *  http://localhost/*.ocaml in your browser for the local apache2 
 */

#include "stdio.h"
#include "string.h"
#include "libgen.h"
#include "stdlib.h"
#include "httpd.h"
#include "http_config.h"
#include "http_protocol.h"
#include "apr_strings.h"

// to avoid using strncpy directly
char *subString(char *someString, int n) 
{
   char *new = malloc(sizeof(char)*n+1);
   strncpy(new, someString, n);
   new[n] = '\0';
   return new;
}

// return a random number
int random_number (int range) {
  srand ((unsigned int)time((time_t *) NULL));
  return (int)((double)rand() / ((double)RAND_MAX + 1) * range);
}

//char *random_filename (request_rec *r, char *tmp_dir, char *fileext) {
//  return ap_rprintf (r->pool, "%s/%d-%04d.%s", tmp_dir, time(NULL), random_number (4095), fileext);
//}

typedef struct {
  const char* key;
  const char* value;
} keyValuePair;

// for POST - produce a file (random name
write_post(request_rec* r,FILE *f) {
  apr_array_header_t *pairs = NULL;
  apr_off_t len;
  apr_size_t size;
  int res;
  int i = 0;
  char *buffer;
  keyValuePair* kvp;
  res = ap_parse_form_data(r, NULL, &pairs, -1, HUGE_STRING_LEN);
  if (res != OK || !pairs) return NULL;
  kvp = apr_pcalloc(r->pool, sizeof(keyValuePair) * (pairs->nelts + 1));
  while (pairs && !apr_is_empty_array(pairs)) {
    ap_form_pair_t *pair = (ap_form_pair_t *) apr_array_pop(pairs);
    apr_brigade_length(pair->value, 1, &len);
    size = (apr_size_t) len;
    buffer = apr_palloc(r->pool, size + 1);
    apr_brigade_flatten(pair->value, buffer, &size);
    buffer[len] = 0;
    kvp[i].key=apr_pstrdup(r->pool,pair->name);
    kvp[i].value=buffer;
    i++;
  }
  int n=0;
  for (n=i-1;n>=0;n--)
    fprintf(f,"%s=%s\n",kvp[n].key,kvp[n].value); // write in the file
}

static int ocaml2_handler(request_rec *req)
{
  // if it is not a ocaml handler, declined
  if (strcmp(req->handler, "ocaml2"))
    {
      return DECLINED;
    }
  
  FILE *fd;
  char command[255]="/opt/mod_ocaml2/preproc.sh "; // the bash script command
  strcat(command,req->filename);
  char *sep;
  sep = strchr(command, ';'); // cut if there are ; avoid exécution of several commands! security
  if (sep != NULL) {
    *sep = '\0';
  }
  strcat(command," ");
  strcat(command,req->method);
  strcat(command," ");
  // production of the POST data file
  char post_file[255];
  char *dir=dirname(req->filename);
  sprintf(post_file,"%s/%d-%d",dir,time(NULL),random_number(1000000));
  //ap_rprintf(req,"fichier post %s<br/>",post_file);
  FILE *fp;
  fp=fopen(post_file,"w+"); // create the file to put GET/POST data
  if (strcmp(req->method,"GET") == 0)
    {
      if (req->args)
	{
	  //ap_rprintf(req,"les arguments: %s<br/>",req->args);
	  fprintf(fp,"%s",req->args);
	}
    }
  else
    {
      write_post(req,fp);  
    }
  fclose(fp);
  strcat(command,post_file);
  strcat(command," 2>&1");
      
  //command=/opt/mod_ocaml2/preproc.sh <page.ocaml> <method (GET or POST)> <name of the file containing data>
  //ap_rprintf(req,"la commande est:%s<br/>",command);
  fd = popen(command, "r"); // exécution of the script
 
  if (!fd) return 1;
  
  char   buffer[256];
  size_t chread;
  // String to store entire command contents in 
  size_t comalloc = 256;
  size_t comlen   = 0;
  char  *comout   = malloc(comalloc);
  
  while ((chread = fread(buffer, 1, sizeof(buffer), fd)) != 0) {
    if (comlen + chread >= comalloc) {
      comalloc *= 2;
           comout = realloc(comout, comalloc);
    }
    memmove(comout + comlen, buffer, chread);
    comlen += chread;
  }
    
    if (!req->header_only) { // data to send to browser
      ap_rputs(comout,req);
      free(comout);
      pclose(fd);
   } 

  req->content_type = "text/html;charset=UTF-8"; // tell to browser the type of document
  return OK;
}

static void ocaml2_register_hooks(apr_pool_t *p)
{
  printf("\n ** ocaml2_register_hooks  **\n\n");
  ap_hook_handler(ocaml2_handler, NULL, NULL, APR_HOOK_MIDDLE);
}

// see https://httpd.apache.org/docs/2.4/developer/modguide.html

module AP_MODULE_DECLARE_DATA ocaml2_module = {
  STANDARD20_MODULE_STUFF, 
  NULL, /* create per-dir    config structures */
  NULL, /* merge  per-dir    config structures */
  NULL, /* create per-server config structures */
  NULL, /* merge  per-server config structures */
  NULL, /* table of config file commands       */
  ocaml2_register_hooks  /* register hooks */
};
