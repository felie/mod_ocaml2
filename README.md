# What ?

mod_ocaml2 allows to use ocaml2 as a preprocessor HTML with apache2, as with php, with \<?ocaml ?\>.

# Fist version

It is a minimal version. 
mod_ocaml (https://github.com/eatonphil/mod_ocaml) was a true module whole in C. But it was for apache 1.3.

The present module passes the useful data to a bash script whose data it retrieves.

If someone want to code this bash script in C, he's welcome!

# Principle

mod_ocaml2 uses preproc.sh

"preproc.sh \<method (GET or POST)\> \<random name of a file which contains data\>"

- if an up to date compiled version of the script doesn't exists
    - transforms the human readeable script for ocaml
    - compile the script
- execute the compiled version (return the result of standard output to mod_ocaml2)

# Installation

## The preproc.sh file

- create a directory for preproc (default is /opt/mod_ocaml2). If you want another, modify the path in mod_ocaml2.c
- put preproc.sh in it
- give it to apache user (www-data)

simply do

$ sudo mkdir /opt/mod_ocaml2;sudo cp preproc.sh config /opt/mod_ocaml2;sudo chown -R www-data.www-data /opt/mod_ocaml2

## Compilation

### apxs

apxs is required

In Debian/Ubuntu, install it with

$ sudo apt-get install apache2-dev

### Compilation and installation of the module

sudo apxs -c -i mod_ocaml2.c

### Declaration ot the module

Add these directives to apache2.conf:

    LoadModule ocaml2_module /usr/lib/apache2/modules/mod_ocaml2.so 
       <FilesMatch ".+\.ocaml">
         SetHandler ocaml2
       </FilesMatch>

## Apache2 restarting

$ sudo service apache2 restart

# Configuration

In the config (by default in /opt/mod_ocaml2

- By default debug is false
- By default compile is true

# Extensions

the mod_univ (soon) is a project to extend this principle to any language

Given a config file, the same module (with a different name and a different config file) can deal with any language.

You can have files with \<?python ?\> blocks in it.

You can have file with \<?cobol ?\> blocks in it, etc.

